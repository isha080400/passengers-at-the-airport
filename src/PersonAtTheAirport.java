public class PersonAtTheAirport {
    String nameOfTheTraveller;
    double weightOfTheLuggage;

    PersonAtTheAirport(String name, double weight) {
        nameOfTheTraveller = name;
        weightOfTheLuggage = weight;
    }

    public Double getWeightOfTheLuggage() {
        return this.weightOfTheLuggage;
    }

    public String getNameOfTheTraveller() {
        return this.nameOfTheTraveller;
    }

    public void calculateExtraCost(){
        double allowedWeight = 15;

        double extraWeight = this.getWeightOfTheLuggage() - allowedWeight;
        double totalExtraCost = 0, extraCostPerKG = 500;


        try {
            if (extraWeight <= 0.2) {
                throw new Exception();
            }
            totalExtraCost += extraCostPerKG * extraWeight;
        } catch (Exception e) {
            System.out.println("The extra weight is small for " + this.getNameOfTheTraveller() + ", hence can be ignored");
        } finally {
            System.out.println("Extra cost is: " + totalExtraCost);
        }

    }

    public static void main(String[] args) {
        PersonAtTheAirport Bhavya = new PersonAtTheAirport("Bhavya", 15.1);
        PersonAtTheAirport Shruti = new PersonAtTheAirport("Shruti", 18);

        Bhavya.calculateExtraCost();
        Shruti.calculateExtraCost();
    }



}
